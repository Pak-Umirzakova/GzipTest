﻿using System;

namespace GZipTest
{
	class Program
	{
		static void Main(string[] args)
		{
			string isCompress = String.Empty;
			string sourceFile = String.Empty;
			string targetFile = String.Empty;

			if (args.Length == 3)
			{
				//сжатие / разжатие
				isCompress = args[0];
				//исходный файл
				sourceFile = args[1];
				//выходной файл
				targetFile = args[2];
			}
			else
			{
				Console.WriteLine("Параметры введены не корректно!\n Введите параметры по принципу:\nGZipTest.exe compress/decompress [имя исходного файла] [имя результирующего файла]");
				return;
			}

			IGZip gZip = new GZip(sourceFile, targetFile);
			switch (isCompress.ToLower())
			{
				case "compress":
					{
						// создание сжатого файла
						gZip.Compress();
						break;
					}
				case "decompress":
					{
						//чтение из сжатого файла
						gZip.Decompress();
						break;
					}
				default:
					{
						Console.WriteLine("Команда архивации/деархивации введена некорректно!");
						return;
						break;
					}
			}
		}
	}
}