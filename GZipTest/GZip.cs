﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Threading;

namespace GZipTest
{
    internal class GZip : IGZip
    {
        private const long BufferSize = 1024 * 1024;
        private static byte[] _buffer = new byte[BufferSize]; // 1MB
        private static bool IsCompressed;
        private const int MaxThreadsCount = 250;
        private readonly Regex _regex = new Regex(@"\s*\[([^]]+)\]");

        private string _sourceFilePath;
        private string _targetFilePath;
        public GZip(string sourceFile, string targetFile)
        {
            _sourceFilePath = sourceFile ?? throw new InvalidOperationException();
            _targetFilePath = targetFile ?? throw new InvalidOperationException();
        }

        public void Compress()
        {
            try
            {
                Match matchSource = _regex.Match(_sourceFilePath ?? throw new InvalidOperationException());
                _sourceFilePath = @matchSource.Groups[1].Value;

                Match matchTarget = _regex.Match(_targetFilePath ?? throw new InvalidOperationException());
                _targetFilePath = @matchTarget.Groups[1].Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Выходной файл введен некорректно!\n" + ex.Message);
            }
            IsCompressed = false;
            ReadSourceFile();
        }

        public void Decompress()
        {
            try
            {
                Match matchSource = _regex.Match(_sourceFilePath);
                _sourceFilePath = @matchSource.Groups[1].Value;

                Match matchTarget = _regex.Match(_targetFilePath ?? throw new InvalidOperationException());
                _targetFilePath = @matchTarget.Groups[1].Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Выходной файл введен некорректно!\n" + ex.Message);
            }
            IsCompressed = true;
            ReadSourceFile();
        }

        private void ReadSourceFile()
        {
            try
            {
                // поток для чтения исходного файла
                using (FileStream sourceStream = new FileStream(_sourceFilePath, FileMode.OpenOrCreate))
                {
                    if (sourceStream.Length <= 0)
                    {
                        Console.WriteLine("Невозможно прочитать исходный файл!");
                        return;
                    }

                    CreateTargetFile(sourceStream, IsCompressed);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при чтении файла.\n" + ex.Message);
            }
        }

        private void CreateTargetFile(FileStream sourceStream, bool isCompressed)
        {
            try
            {
                // поток для записи сжатого файла
                using (FileStream targetStream = File.Create(_targetFilePath))
                {
                    if (isCompressed == true)
                        CreateGzipStreamToDecompress(sourceStream, targetStream);
                    else
                        CreateGzipStreamToCompress(sourceStream, targetStream);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при создании выходного файла файла.\n" + ex.Message);
            }
        }

        private void CreateGzipStreamToCompress(FileStream sourceStream, FileStream targetStream)
        {
            try
            {
                // поток архивации
                using (GZipStream compressionStream = new GZipStream(targetStream, CompressionMode.Compress))
                {
                    //количество прочитанных байт
                    long bytesRead = 0;
                    int i = 0;
                    object locker = new object();
                    Thread[] threads = new Thread[MaxThreadsCount];
                    while (sourceStream.Length > bytesRead)
                    {
                        threads[i] = new Thread(delegate ()
                        {
                        // этот фрагмент кода - цикл, - будет исполняться в отдельных потоках                               
                        lock (locker)
                            {
                                if ((sourceStream.Length - bytesRead) < _buffer.Length 
                                    && (sourceStream.Length - bytesRead) > 0)
                                {
                                    int resudial = (int)(sourceStream.Length - bytesRead);
                                    _buffer = new byte[resudial];
                                }
                                int readLength = sourceStream.Read(_buffer, 0, _buffer.Length);
                                bytesRead += readLength;
                                compressionStream.Write(_buffer, 0, readLength);
                            }
                        });
                        threads[i].Start();
                        i++;
                        // Если количество выделенных потоков достигло заданного максимума, то ожидаем завершения потоков
                        if (i == MaxThreadsCount - 1)
                        {
                            for (int j = 0; j < i; ++j)
                                threads[j].Join();
                            i = 0;
                        }
                    }
                    // блокирующее ожидание завершения потоков
                    for (int j = 0; j < i; ++j)
                        threads[j].Join();
                    compressionStream.Flush();
                    targetStream.Flush();
                }
                Console.WriteLine("Файл заархивирован.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при архивировании файла.\n" + ex.Message);
            }
        }

        private void CreateGzipStreamToDecompress(FileStream sourceStream, FileStream targetStream)
        {
            try
            {
                // поток разархивации
                using (GZipStream decompressionStream = new GZipStream(sourceStream, CompressionMode.Decompress))
                {
                    //количество прочитанных байт
                    long bytesRead = 0;
                    int i = 0;
                    object locker = new object();
                    Thread[] threads = new Thread[MaxThreadsCount];
                    while (sourceStream.Length > bytesRead)
                    {
                        threads[i] = new Thread(delegate ()
                        {
                        // этот фрагмент кода - цикл, - будет исполняться в отдельных потоках                               
                        lock (locker)
                            {
                                if ((sourceStream.Length - bytesRead) < _buffer.Length &&
                                    (sourceStream.Length - bytesRead) > 0)
                                {
                                    int resudial = (int)(sourceStream.Length - bytesRead);
                                    _buffer = new byte[resudial];
                                }
                                int readLength = decompressionStream.Read(_buffer, 0, _buffer.Length);

                                targetStream.Write(_buffer, 0, readLength);
                                bytesRead += readLength;
                            }
                        });
                        threads[i].Start();
                        i++;

                        // Если количество выделенных потоков достигло заданного максимума, то ожидаем завершения потоков	
                        if (i == MaxThreadsCount - 1)
                        {
                            for (int j = 0; j < i; ++j)
                                threads[j].Join();
                            i = 0;
                        }
                    }
                    // блокирующее ожидание завершения потоков
                    for (int j = 0; j < i; ++j)
                        threads[j].Join();
                    decompressionStream.Flush();
                    targetStream.Flush();
                }
                Console.WriteLine("Файл деархивирован.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при деирхаивировании файла.\n" + ex.Message);
            }
        }
    }
}