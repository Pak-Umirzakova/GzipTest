﻿using System.IO;

namespace GZipTest
{
    internal interface IGZip
    {
        void Compress();
        void Decompress();
    }
}
